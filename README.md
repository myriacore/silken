# SilkeN

SilkeN is an [Esoteric Programming Language](https://esolangs.org/wiki/Esoteric_programming_language) inspired by [Dark](https://esolangs.org/wiki/Dark). It's designed for worldbuilding, so an author can describe the world and environment their characters will be in, and the program will generate a description of a character interacting with said world. 

**WARNING:** This is *pre-alpha software* (pre-alpha in the sense that it does not yet exist). Use at your own risk! There will be bugs! 